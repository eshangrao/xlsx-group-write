use time::{
    macros::{format_description, offset},
    OffsetDateTime,
};
/// 获取当前时间
pub fn get_now_date() -> String {
    // 中国为UTF+8
    let date_format = format_description!("[year][month][day]");
    let now = OffsetDateTime::now_utc().to_offset(offset!(+8)).date();
    now.format(&date_format).unwrap()
}