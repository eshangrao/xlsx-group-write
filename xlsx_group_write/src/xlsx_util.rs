use umya_spreadsheet::{
    Border, HorizontalAlignmentValues, NumberingFormat, Style, VerticalAlignmentValues, Worksheet,
};

#[derive(Debug)]
pub struct XlsxWriterTool;
impl XlsxWriterTool {
    /// 设置单元格内容为：字符
    ///
    /// # 参数说明
    ///
    /// * sheet 要写入的excel表格
    /// * col 列序号,第一列序号为1
    /// * row 行序号，第一行序号为1
    /// * value 要设置的值
    pub fn set_excel_cell_value_str(sheet: &mut Worksheet, col: u32, row: u32, value: &str) {
        sheet.get_cell_mut((&col, &row)).set_value_string(value);
        let style = sheet.get_style_mut((&col, &row));
        Self::set_border_and_center_align(style);
    }

    #[allow(dead_code)]
    /// 设置单元格内容为：数字
    ///
    /// # 参数说明
    ///
    /// * sheet 要写入的excel表格
    /// * col 列序号,第一列序号为1
    /// * row 行序号，第一行序号为1
    /// * value 要设置的值
    pub fn set_excel_cell_value_number<T>(sheet: &mut Worksheet, col: u32, row: u32, value: T)
    where
        f64: From<T>,
    {
        sheet.get_cell_mut((&col, &row)).set_value_number(value);
        let style = sheet.get_style_mut((&col, &row));
        Self::set_border_and_center_align(style);
    }
    /// 设置单元格边框并设置水平和垂直居中
    pub fn set_border_and_center_align(style: &mut Style) {
        let border = style.get_borders_mut();
        // add bottom border
        border
            .get_bottom_mut()
            .set_border_style(Border::BORDER_THIN);
        // add top border
        border.get_top_mut().set_border_style(Border::BORDER_THIN);
        // add left border
        border.get_left_mut().set_border_style(Border::BORDER_THIN);
        // add right border
        border.get_right_mut().set_border_style(Border::BORDER_THIN);
        let alignment = style.get_alignment_mut();
        alignment.set_horizontal(HorizontalAlignmentValues::Center);
        alignment.set_vertical(VerticalAlignmentValues::Center);
        style
            .get_number_format_mut()
            .set_format_code(NumberingFormat::FORMAT_TEXT);
    }
}
