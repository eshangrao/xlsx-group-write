# [xlsx_group_write](https://crates.io/crates/xlsx_group_write) 派生宏

无需手工实现XlsxGroupWrite即可快速xlsx写入

## 用法

```rust
use xlsx_group_write_macro_derive::XlsxGroupWriteQuicker;

#[derive(XlsxGroupWriteQuicker)]
#[xlsx_group_write(
    // 快捷行数据写入定义
    // 各列以逗号分隔，可使用两个函数参数：
    // * line_index 为行号
    // * &self 为待写入的数据
    // 每个列数据定义为value:type
    // * value 要写入的数据，可为表达式，如：line_index-1，表示当前待写入数据行号-1
    // * type 数据类型,number/string，不指定则为string类型
    line_writer_simple = "line_index-1:number,&self.name,&self.tel,&self.dep",
    // 个性化行数据写入,调用对应函数进行写入行数据
    // 与line_writer_simple互斥，line_writer_simple优先
    // 该函数签名：fn write_xlsx_line(line_data:&MyAdvanceData, sheet: &mut Worksheet, line_index: u32,) -> Option<XlsxLineAdvanceWriterResult>
    line_writer_advance = "write_xlsx_line",
    // 个性化行数据写入,调用对应函数进行写入行数据，同时附加额外的参数
    // 与line_writer_simple互斥，line_writer_simple优先
    // 该函数签名：fn write_xlsx_line(line_data:&MyAdvanceData, sheet: &mut Worksheet, line_index: u32,extra_arg: Option<Self::ExtraArgType>,) -> Option<XlsxLineAdvanceWriterResult>
    line_writer_advance_with_extra_arg = "write_xlsx_line_with_extra_arg",
    // 分组id指定，如无需分组输出xlsx文件，可不指定。示例为使用写入数据的dep属性,
    group_maker = "&self.dep",
    // 快捷表头设置，各列以逗号分隔
    template_simple = "序号,姓名,手机,部门"
    // 通过模板文件生成表头，设置列宽等
    // 设置格式：模板文件位置,数据起始写入行行号（第一行序号从1开始）
    // 与template_simple、template_getter互斥，template_simple优先
    template_advance = "/home/feiy/Desktop/temp.xlsx,4",
    // 通过函数获取模板信息
    // 用于模板文件动态确定场景
    // 与template_simple、template_advance互斥，template_simple优先、template_advance次之
    // 该函数签名：fn get_template() -> xlsx_group_write::data::XlsxInitTemplet;     
    template_getter = "my_template_getter",
    // 表格个性化信息添加器设置
    // 在表格中添加自定义个性化信息
    // 该函数签名：fn add_custom_info(sheet: &mut Worksheet, group_id: &str) 
    custom_info_adder = "add_custom_info",
    // 输出的xlsx文件名前缀，含有完整路径，实际输出会自动添加.xlsx后缀
    output_file_name_simple = "/tmp/test",
    // 设置自动在输出文件名中添加当前日期
    // 需与output_file_name_simple一起使用，
    // 最终生成文件名称为类似：/tmp/test-2023-04-23.xlsx
    output_file_name_add_date = "true",
    // 个性化文件名称生成器，将调用指定的函数获取输出文件名称
    // 该函数签名：fn get_output_file_name_advance(group_id: &str) -> String 
    output_file_name_advance = "get_output_file_name_advance",
    // 个性化信息类型
    // 输出xlsx文件时，向文件名称生成器函数和custom_info_add传入个性化信息
    extra_arg_type ="String",

)]
```

## 示例 1

自动生成表头，各行数据自动写入

```rust
use xlsx_group_write::prelude.*;

#[derive(XlsxGroupWriteQuicker)]
#[xlsx_group_write(
    line_writer_simple = "line_index-1:number,&self.name,&self.tel,&self.dep",
    group_maker = "&self.dep",
    template_simple = "序号,姓名,手机,部门",
    output_file_name_simple = "/tmp/test"
)]
struct MySimpleData {
    pub name: String,
    pub tel: String,
    pub dep: String,
}
impl MySimpleData {
    pub fn new(name: &str, tel: &str, dep: &str) -> Self {
        Self {
            name: name.into(),
            tel: tel.into(),
            dep: dep.into(),
        }
    }
}

#[test]
fn test_simple() {
    // 初始化待导出数据
    let data = vec![
        MySimpleData::new("张三", "185xxxx2228", "网金部"),
        MySimpleData::new("李四", "185xxxx2229", "运管部"),
        MySimpleData::new("王二", "185xxxx2230", "网金部"),
    ];
    // 导出数据到xlsx文件
    // 自动按照部门分别导出,同时还汇总导出一个汇总xlsx文件
    // 成功导出返回对应的文件信息和分组id
    let resp = MySimpleData::write2xlsx_all(&data);
    println!("resp:{resp:#?}");
}

```

## 示例 2

以某xlsx文件为模板，生成表标题和表头，并在表格添加自定义信息，各行数据自动写入

```rust
use xlsx_group_write_macro_derive::XlsxGroupWriteQuicker;
use xlsx_group_write::XlsxGroupWrite;
use xlsx_group_write::data::{XlsxLineWriterModel,XlsxColValueType,XlsxColValue,XlsxInitTemplet,OutputFileNameSimpleGetter};


#[derive(XlsxGroupWriteQuicker)]
#[xlsx_group_write(
    line_writer_simple = "line_index-1:number,&self.name,&self.tel,&self.dep",
    group_maker = "&self.dep",
    template_advance = "/home/feiy/Desktop/temp.xlsx,4",
    custom_info_adder = "Self::add_custom_info",
    output_file_name_simple = "/tmp/test",
)]
struct MySimpleDataWithTemplate {
    pub name: String,
    pub tel: String,
    pub dep: String,
}
impl MySimpleDataWithTemplate {
    pub fn new(name: &str, tel: &str, dep: &str) -> Self {
        Self {
            name: name.into(),
            tel: tel.into(),
            dep: dep.into(),
        }
    }

    /// 对导出的xlsx文件进行一些个性化修改
    /// 比如需要在第二行插入对应分组名称，插入报表数据范围等信息
    ///
    /// 将在xlsx初始化完成后自动调用该方法
    ///
    /// # 参数说明
    ///
    /// * sheet 要修改的xlsx工作表
    /// * group_id 该xlsx对应的分组id
    fn add_custom_info(sheet: &mut Worksheet, group_id: &str) {
        sheet
            .get_cell_mut((&1, &2))
            .set_value_string(&format!("{group_id}，报表日期:2023年4月20日"));
    }
}

#[test]
fn test_simple_with_template() {
    // 初始化待导出数据
    let data = vec![
        MySimpleDataWithTemplate::new("张三", "185xxxx2228", "网金部"),
        MySimpleDataWithTemplate::new("李四", "185xxxx2229", "运管部"),
        MySimpleDataWithTemplate::new("王二", "185xxxx2230", "网金部"),
    ];
    // 导出数据到xlsx文件
    // 自动按照部门分别导出,同时还汇总导出一个汇总xlsx文件
    // 成功导出返回对应的文件信息和分组id
    let resp = MySimpleDataWithTemplate::write2xlsx_all(&data);
    println!("resp:{resp:#?}");
}
```

## 示例 3

自动生成表头，各行数据调用个性化写入函数write_xlsx_line写入

```rust
use umya_spreadsheet::Worksheet;
use xlsx_group_write::data::{
    OutputFileNameSimpleGetter, XlsxColValue, XlsxColValueType, XlsxInitTemplet,
    XlsxLineWriterModel, XlsxLineAdvanceWriterResult,
};
use xlsx_group_write::XlsxGroupWrite;
use xlsx_group_write::xlsx_util::XlsxWriterTool;
use xlsx_group_write_macro_derive::XlsxGroupWriteQuicker;

#[derive(XlsxGroupWriteQuicker)]
#[xlsx_group_write(
    line_writer_advance = "Self::write_xlsx_line",
    group_maker = "&self.dep",
    template_simple = "序号,姓名,手机,部门",
    output_file_name_simple = "/tmp/test",
)]
struct MyAdvanceData {
    pub name: String,
    pub tel: String,
    pub dep: String,
}
impl MyAdvanceData {
    pub fn new(name: &str, tel: &str, dep: &str) -> Self {
        Self {
            name: name.into(),
            tel: tel.into(),
            dep: dep.into(),
        }
    }
    /// 个性化写入行数据
    fn write_xlsx_line(
        line_data:&MyAdvanceData,
        sheet: &mut Worksheet,
        line_index: u32,
    ) -> Option<XlsxLineAdvanceWriterResult> {
        XlsxWriterTool::set_excel_cell_value_number(sheet, 1, line_index, line_index - 1);
        XlsxWriterTool::set_excel_cell_value_str(sheet, 2, line_index, &format!("ad-{}",&line_data.name));
        XlsxWriterTool::set_excel_cell_value_str(sheet, 3, line_index, &line_data.tel);
        XlsxWriterTool::set_excel_cell_value_str(sheet, 4, line_index, &line_data.dep);
        None
    }
}

#[test]
fn test_advance_write() {
    // 初始化待导出数据
    let data = vec![
        MyAdvanceData::new("张三", "185xxxx2228", "网金部"),
        MyAdvanceData::new("李四", "185xxxx2229", "运管部"),
        MyAdvanceData::new("王二", "185xxxx2230", "网金部"),
    ];
    // 导出数据到xlsx文件
    // 自动按照部门分别导出,同时还汇总导出一个汇总xlsx文件
    // 成功导出返回对应的文件信息和分组id
    let resp = MyAdvanceData::write2xlsx_all(&data);
    println!("resp:{resp:#?}");
}
```

## 示例 4

自动生成表头，各行数据自动写入,输出文件名称通过函数获取

```rust
use umya_spreadsheet::Worksheet;
use xlsx_group_write::data::{
    OutputFileNameSimpleGetter, XlsxColValue, XlsxColValueType, XlsxInitTemplet,
    XlsxLineWriterModel, XlsxLineAdvanceWriterResult,
};
use xlsx_group_write::XlsxGroupWrite;
use xlsx_group_write::xlsx_util::XlsxWriterTool;
use xlsx_group_write_macro_derive::XlsxGroupWriteQuicker;

#[derive(XlsxGroupWriteQuicker)]
#[xlsx_group_write(
    line_writer_simple = "line_index-1:number,&self.name,&self.tel,&self.dep",
    group_maker = "&self.dep",
    template_simple = "序号,姓名,手机,部门",
    output_file_name_advance = "Self::get_output_file_name_advance",
)]
struct MyFileNameData {
    pub name: String,
    pub tel: String,
    pub dep: String,
}
impl MyFileNameData {
    pub fn new(name: &str, tel: &str, dep: &str) -> Self {
        Self {
            name: name.into(),
            tel: tel.into(),
            dep: dep.into(),
        }
    }

    /// 完全自定义的输出文件名称名称生成器
    ///
    /// 参数说明:
    ///
    /// * groupt_id ：当期数据的分组id
    #[allow(dead_code)]
    fn get_output_file_name_advance(group_id: &str) -> String {
        format!("/tmp/f-{group_id}.xlsx")
    }

}

#[test]
fn test_advance_file_name() {
    // 初始化待导出数据
    let data = vec![
        MyFileNameData::new("张三", "185xxxx2228", "网金部"),
        MyFileNameData::new("李四", "185xxxx2229", "运管部"),
        MyFileNameData::new("王二", "185xxxx2230", "网金部"),
    ];
    // 导出数据到xlsx文件
    // 自动按照部门分别导出,同时还汇总导出一个汇总xlsx文件
    // 成功导出返回对应的文件信息和分组id
    let resp = MyFileNameData::write2xlsx_all(&data);
    println!("resp:{resp:#?}");
}
```

### （四）示例 5

自动生成表头，各行数据个性化写入，传递额外参数到文件名称，写入个性化信息到xlsx表中

```rust
use xlsx_group_write::*;

#[derive(XlsxGroupWriteQuicker)]
#[xlsx_group_write(
    line_writer_advance_with_extra_arg = "Self::write_xlsx_line",
    group_maker = "&self.dep",
    template_simple = "序号,姓名,手机,部门",
    extra_arg_type ="String",
    custom_info_adder = "Self::add_custom_info",
    output_file_name_advance = "Self::get_output_file_name_advance",
)]
struct MySimpleDataWithExtraArg {
    pub name: String,
    pub tel: String,
    pub dep: String,
}
impl MySimpleDataWithExtraArg {
    pub fn new(name: &str, tel: &str, dep: &str) -> Self {
        Self {
            name: name.into(),
            tel: tel.into(),
            dep: dep.into(),
        }
    }
    /// 写入行数据，传入额外参数
    fn write_xlsx_line(
        line_data:&MySimpleDataWithExtraArg,
        sheet: &mut Worksheet,
        line_index: u32,
        extra_arg: Option<&String>,
    ) -> Option<XlsxLineAdvanceWriterResult> {
        XlsxWriterTool::set_excel_cell_value_number(sheet, 1, line_index, line_index - 1);
        XlsxWriterTool::set_excel_cell_value_str(sheet, 2, line_index, &format!("ad-{}",&line_data.name));
        XlsxWriterTool::set_excel_cell_value_str(sheet, 3, line_index, &line_data.tel);
        XlsxWriterTool::set_excel_cell_value_str(sheet, 4, line_index, &line_data.dep);
        if let Some(extra_arg) = extra_arg{
            XlsxWriterTool::set_excel_cell_value_str(sheet, 5, line_index, extra_arg);
        }
        None
    }
    /// 对导出的xlsx文件进行一些个性化修改
    /// 比如需要在第二行插入对应分组名称，插入报表数据范围等信息
    ///
    /// 将在xlsx初始化完成后自动调用该方法
    ///
    /// # 参数说明
    ///
    /// * sheet 要修改的xlsx工作表
    /// * group_id 该xlsx对应的分组id
    fn add_custom_info(sheet: &mut Worksheet, group_id: &str,org_name:Option<&String>) {
        if let Some(org_name) = org_name{
            sheet
                .get_cell_mut((&1, &5))
                .set_value_string(&format!("{org_name} {group_id}，报表日期:2023年4月20日"));
        }else{
            sheet
                .get_cell_mut((&1, &5))
                .set_value_string(&format!("{group_id}，报表日期:2023年4月20日"));
        }
    }
    /// 完全自定义的输出文件名称名称生成器
    ///
    /// 参数说明:
    ///
    /// * groupt_id ：当期数据的分组id
    #[allow(dead_code)]
    fn get_output_file_name_advance(group_id: &str,org_name:Option<&String>) -> String {
        if let Some(org_name) = org_name{
            format!("/tmp/f-{org_name}-{group_id}.xlsx")
        }else{
            format!("/tmp/f-{group_id}.xlsx")
        }
    }
}

#[test]
fn test_simple_with_extra_arg() {
    // 初始化待导出数据
    let data = vec![
        MySimpleDataWithExtraArg::new("张三", "185xxxx2228", "网金部"),
        MySimpleDataWithExtraArg::new("李四", "185xxxx2229", "运管部"),
        MySimpleDataWithExtraArg::new("王二", "185xxxx2230", "网金部"),
    ];
    // 导出汇总数据到xlsx文件
    // 成功导出返回对应的文件信息和分组id
    // 传入个性化参数
    let resp = MySimpleDataWithExtraArg::write2xlsx_merge_only_with_extra_arg(&data,Some(String::from("个性化参数")));
    println!("resp:{resp:#?}");
}
```